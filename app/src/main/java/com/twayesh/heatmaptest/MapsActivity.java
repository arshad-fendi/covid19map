package com.twayesh.heatmaptest;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;

    private DatabaseReference COVIDUserData = FirebaseDatabase.getInstance().getReference("COVID_positions");
    private TileOverlay mOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        askForPermission();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSymptomsDialog();
            }
        });
    }

    private void askForPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        LatLng lastLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 16));
                                    }
                                }
                            });
                } else {
                    Toast.makeText(MapsActivity.this, "Location need to be granted!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        COVIDUserData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.v("wtf", "on data changed: dataSnapshot = " + dataSnapshot);
                List<LatLng> latLngList = new ArrayList<>();

                for(DataSnapshot position : dataSnapshot.getChildren()) {
                    CustomLatLng latLng = position.getValue(CustomLatLng.class);
                    latLngList.add(new LatLng(latLng.getLat(), latLng.getLng()));
                }

                if(latLngList.size() > 0) {
                    updateHeatMap(latLngList);
                } else {
                    clearHeatMap();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            LatLng lastLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 16));
                        }
                    }
                });
    }

    /**
     * Add current location when confirmed the symptoms dialog
     */
    private void addCurrentLocation() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            String id = COVIDUserData.push().getKey();
                            COVIDUserData.child(id).setValue(
                                    new CustomLatLng(location.getLatitude(), location.getLongitude()));
                        }
                    }
                });
    }

    private void updateHeatMap(List<LatLng> list) {
        // Create a heat map tile provider, passing it the latlngs of the police stations.
        HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                .data(list)
                .build();
        // Add a tile overlay to the map, using the heat map tile provider.
        mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
    }

    private void clearHeatMap() {
        if(mOverlay != null) {
            mOverlay.remove();
        }
    }

    private void createSymptomsDialog() {
        final ArrayList selectedSymptoms = new ArrayList();

        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setTitle("How are you today?");
        builder.setMultiChoiceItems(new String[]{"Cough", "Fever", "Tiredness", "Running nose"},
                null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                if (isChecked) {
                    // If the user checked the item, add it to the selected items
                    selectedSymptoms.add(which);
                } else if (selectedSymptoms.contains(which)) {
                    // Else, if the item is already in the array, remove it
                    selectedSymptoms.remove(Integer.valueOf(which));
                }
            }
        });

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK, so save the selectedItems results somewhere
                // or return them to the component that opened the dialog
                createConfirmationDialog(selectedSymptoms);
            }
        });

        builder.create();
        builder.show();
    }

    private void createConfirmationDialog(ArrayList selectedSymptoms) {

        String message = "You seems to be fine today :)";
        switch (selectedSymptoms.size()) {
            case 1:
            case 2:
                message = "Your symptoms are mild, please consider to add them to the map!";
                break;
            case 3:
                message = "You seems to be sick, stay home!\nPlease consider to add them to the map!";
                break;
            case 4:
                message = "You seems to be sick, call the hospital!\nPlease consider to add them to the map!";
                break;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setTitle("Thank you");
        builder.setMessage(message);

        if(selectedSymptoms.size() > 0) {
            builder.setPositiveButton("Add to map", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    addCurrentLocation();
                }
            });
        }

        builder.setNegativeButton("Close", null);

        builder.create();
        builder.show();
    }
}
